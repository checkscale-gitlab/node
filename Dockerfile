FROM node:alpine AS base
# bootstrap alpine using ci-base
RUN wget -O /usr/bin/alpine-bootstrap.sh https://gitlab.com/usvc/images/ci/base/raw/master/shared/alpine-bootstrap.sh \
  && chmod +x /usr/bin/alpine-bootstrap.sh \
  && /usr/bin/alpine-bootstrap.sh \
  && rm -rf /usr/bin/alpine-bootstrap.sh
# typescript
RUN npm install --global typescript ts-node
WORKDIR /
LABEL \
  description="A CI image for Node-based projects" \
  canonical_url="https://gitlab.com/usvc/images/ci/node" \
  license="MIT" \
  maintainer="zephinzer" \
  authors="zephinzer"

FROM base AS docker
RUN wget -O /usr/bin/docker-bootstrap.sh https://gitlab.com/usvc/images/ci/docker/raw/master/shared/docker-bootstrap.sh \
  && chmod +x /usr/bin/docker-bootstrap.sh \
  && /usr/bin/docker-bootstrap.sh \
  && rm -rf /usr/bin/docker-bootstrap.sh
VOLUME [ "/var/run/docker.sock" ]

FROM docker AS gitlab
ENV DOCKER_HOST=tcp://docker:2375/ \
  DOCKER_DRIVER=overlay2
